import { Controller, Get, Query } from '@nestjs/common';

@Controller()
export class SturController {

  @Get("/iptu")
  getIptu(@Query('year') year : string) {
    if (year === '2020') {
      return [
        { inscricao: "10000-0", endereco: "Rua do Destino bom, 321", status: "PAID", billId: 0 }
      ]
    } else if (year === '2021') {
      return [
        { inscricao: "10000-0", endereco: "Rua do Destino bom, 321", status: "PAID", billId: 0 },
        { inscricao: "10001-1", endereco: "Rua do Destino bom, 322", status: "PENDING", billId: 100 },
      ]
    }

    return [];

  }

  @Get("/itr")
  getItr(@Query('year') year : string) {

    if (year === '2020') {
      return [
        { inscricao: "4.282.86", endereco: "Rua Anielo Feula, Bom Destino, MG, 35000-000", area: 2.0, status: "REGULAR", billId: 0 },
        { inscricao: "1.001.32", endereco: "Rua das Mercês, Bom Destino, MG, 35000-000", area: 4.1, status: "IRREGULAR", billId: 100 },
      ]
    } else if (year === '2021') {
      return [
        { inscricao: "4.282.86", endereco: "Rua Anielo Feula, Bom Destino, MG, 35000-000", area: 2.0, status: "REGULAR", billId: 0 },
        { inscricao: "62.320.60", endereco: "Rua Macau do Meio, Bom Destino, MG, 35000-000", area: 8.5, status: "PENDING", billId: 100 },
      ]
    }

    return [];
    
  }



}

import { Module } from '@nestjs/common';
import { APP_GUARD, APP_INTERCEPTOR } from '@nestjs/core';
import { AuthModule } from './auth/auth.module';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { LoggingInterceptor } from './interceptors/logging.interceptor';
import { SturController } from './stur/stur.controller';
import { VaultModule } from './vault/vault.module';
@Module({
  imports: [VaultModule, AuthModule],
  controllers: [SturController],
  providers: [
    { provide: APP_GUARD, useClass: JwtAuthGuard},
    { provide: APP_INTERCEPTOR, useClass: LoggingInterceptor},
  ],
})
export class AppModule {}

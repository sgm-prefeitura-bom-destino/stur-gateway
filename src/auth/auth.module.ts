import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { VaultLoader } from 'src/vault/vault-loader/vault-provider.service';
import { VaultModule } from 'src/vault/vault.module';
import { JwtAuthGuard } from './jwt-auth.guard';
import { JwtModuleRegisterService } from './jwt-module-register/jwt-module-register.service';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports: [
    VaultModule,
    JwtModule.registerAsync({ useClass: JwtModuleRegisterService, imports: [VaultModule]  })
  ],
  providers: [JwtStrategy, JwtAuthGuard],
  exports: [JwtAuthGuard]
})
export class AuthModule { }
